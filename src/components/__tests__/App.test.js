import React from 'react';
import { shallow } from 'enzyme';
import App from 'components/App';
import CommentBox from 'components/CommentBox';
import ComentList from 'components/CommentList';

let wrapped;

beforeEach(() => {
    /**
     * Creates an object for us each time before a test
     * function is called. This can only be accessed by the test
     * functions within this file.
     */
    wrapped = shallow(<App />);
})

it('shows a comment box', () => {
    /**
     * Expect the object and find the commponent supplied.
     * We would like to know there is only one instance of
     * commentbox
     */
    expect(wrapped.find(CommentBox).length).toEqual(1);
    
});

it('shows a comment list', () => {
   expect(wrapped.find(ComentList).length).toEqual(1); 
});